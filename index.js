
var texturePath = 'textures/';
var PRArtTexturePath = require('programmerart-textures')('');
var createGame = require('voxel-engine');
var highlight = require('voxel-highlight');
var fly = require('voxel-fly');

var container = document.querySelector('#container');
var gameConfig = {
	mineSize: 3.0, // r of cube
	materials: ['bc1.png', 'bc2.png', 'bc3.png'],
	controls: { discreteFire: true },
	eraseDuration: 1000 // in ms
};

var game = createGame({
    texturePath: texturePath,
    materials: gameConfig.materials,
    generate: function(x,y,z) {
    	var xnorm = Math.abs(x / gameConfig.mineSize);
    	var ynorm = Math.abs(y / gameConfig.mineSize);
    	var znorm = Math.abs(z / gameConfig.mineSize);
    	var materialIndex = Math.floor(
    			(1 - Math.max(xnorm, ynorm, znorm)) * (gameConfig.materials.length - 1) + 1);

    	return (xnorm <= 1 && ynorm <= 1 && znorm <= 1) ? materialIndex : 0;
  	},
});

var blockPosErase = null, erasedLastTime = new Date().getTime();
var hl = game.highlighter = highlight(game, { color: 0xff0000 })
hl.on('highlight', function (voxelPos) { blockPosErase = voxelPos })
hl.on('remove', function (voxelPos) { blockPosErase = null })

var createPlayer = require('voxel-player')(game);
var player = createPlayer(texturePath + 'player.png'); // FIXME texture

var makeFly = fly(game);
var fly = makeFly(player);
player.possess();
player.yaw.position.set(0, gameConfig.mineSize + 1, 0);

window.game = game; // for debugging
game.appendTo(container);

game.on('fire', function (target, state) {
	var date = new Date();
	if (blockPosErase && // TODO check number of neighbors
		erasedLastTime + gameConfig.eraseDuration < date.getTime()) { // erase block
		erasedLastTime = date.getTime();
		game.setBlock(blockPosErase, 0);
		// TODO give money, etc.
	}
});

window.addEventListener('keydown', function (ev) {
    if (ev.keyCode === 'R'.charCodeAt(0)) {
        player.toggle();
    }
});

// TODO install voxel-server, voxel-client